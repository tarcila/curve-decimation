#pragma once

#include <QObject>
#include <QOpenGLDebugLogger>

class OpenGLLogger;

class OpenGLLoggerPrivate : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(OpenGLLoggerPrivate)

    OpenGLLogger* const q_ptr;
    Q_DECLARE_PUBLIC(OpenGLLogger)

    QOpenGLDebugLogger m_logger;

public:
    explicit OpenGLLoggerPrivate(OpenGLLogger* parent);
    virtual ~OpenGLLoggerPrivate();

    void initialize();
    void finish();


 public Q_SLOTS:
    void logMessage(const QOpenGLDebugMessage& message);
};
