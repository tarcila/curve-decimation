#version 450 core

layout(binding=0) uniform sampler2D texture;

layout(location=0) out vec4 color;

void main() {
#if 1
  color = texelFetch(texture, ivec2(gl_FragCoord.xy), 0) * 3.0;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(-1, -1)) * 0.25;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(-0, -1)) * 0.5;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2( 1, -1)) * 0.25;

  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(-1, 0)) * 0.75;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2( 1, 0)) * 0.75;

  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(-1, 1)) * 0.25;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(-0, 1)) * 0.75;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2( 1, 1)) * 0.25;

  color /= (3.0 + 0.5 + 0.75 + 0.5 + 0.75 + 0.75 + 0.5 + 0.75 + 0.5);
#elif 0
  color = texelFetch(texture, ivec2(gl_FragCoord.xy), 0) * 2;
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(1.0, 1.0));
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(0.0, 1.0));
  color += texelFetchOffset(texture, ivec2(gl_FragCoord.xy), 0, ivec2(1.0, 0.0));
  color /= 5.0;
#elif 0
  ivec2 coord = ivec2(gl_FragCoord.xy);
  ivec2 localCoord = ivec2(coord % 2);
  color = texelFetch(texture, (coord / 2) * 2 + 1 - localCoord, 0);
#else
  color = texelFetch(texture, ivec2(gl_FragCoord.xy), 0);
#endif
}
