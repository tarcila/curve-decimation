#include "Viewer3D.hpp"
#include "Viewer3DPrivate.hpp"

#include <QQuickWindow>

Viewer3D::Viewer3D(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new Viewer3DPrivate(this))
{
    connect(this, SIGNAL(windowChanged(QQuickWindow*)), this, SLOT(handleWindowChanged(QQuickWindow*)));
    connect(this, SIGNAL(dataSourceChanged(DataSource*)), this, SLOT(handleDataSourceChanged(DataSource*)));
}

Viewer3D::~Viewer3D()
{
}

qreal Viewer3D::from() const
{
    return m_from;
}

qreal Viewer3D::to() const
{
    return m_to;
}

void Viewer3D::handleWindowChanged(QQuickWindow *window)
{
    Q_D(Viewer3D);

    if (window) {
        d->doConnect();
        window->setClearBeforeRendering(false);
    } else {
        d->doDisconnect();
    }
}

void Viewer3D::handleDataSourceChanged(DataSource *dataSource)
{
    disconnect(this, SLOT(update()));
    if (dataSource)
        connect(dataSource, SIGNAL(updated()), this, SLOT(update()));
}
