import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.0

import mGlib 1.0

Item {
    id: mainWindow
    visible: true
    width: 800
    height: 600

    Viewer3D {
        id: viewer
        x: 0
        width: parent.width
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        from: from.value
        to: to.value
        resolution: width
        antialiasing: true
        dataSource: DataSource {
            type: DataSource.Sine
            size: sampleSize.value
        }
    }

    ProgressBar {
        id: progressBar
        visible: false
        width: parent.width * 0.25
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }

    Row {
        id: row1
        y: 600
        anchors.leftMargin: 2
        anchors.bottomMargin: 2
        visible: true
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        Slider {
            id: from
            width: 100
            value: 0
            minimumValue: 0
            maximumValue: to.value - 1
            stepSize: 1
        }

        Slider {
            id: to
            width: 100
            minimumValue: from.value + 1
            maximumValue: sampleSize.value
            stepSize: 1
            value: sampleSize.maximumValue
        }

        Slider {
            id: sampleSize
            width: 100
            value: 4096
            minimumValue: viewer.width
            maximumValue: 8192
            stepSize: 1
        }
    }
}
