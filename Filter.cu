#include "Filter.cu.h"

#include <cuda_runtime.h>

#define BLOCK_SIZE 256

__global__ void cudaComputeReduction(const int outputSize, PositionAndColor* output, int* weights, const int inputSize, const float2* input, const float2 inputRange, const int colormapSize, const float4* colormap) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;

    int outputBucket = -1;
    float2 position;
    uchar4 color;

    if (ix < inputSize)
    {
        float2 i = input[ix];
        float normalizedX = (i.x - inputRange.x) / (inputRange.y - inputRange.x);
        outputBucket = __float2int_rn(outputSize * normalizedX  - 0.5);
        position.x = normalizedX;
        position.y = i.y;
        color = make_uchar4(255, 255, 255, 255);
    }

    //int nextOutputBucket = __shlf_down(outputBucket, 1);
    //if (nextOutputBucket = )

    if (outputBucket >= 0) {
        output[outputBucket].position.x = __float2half_rn(position.x);
        output[outputBucket].position.y = __float2half_rn(position.y);
        output[outputBucket].color = make_uchar4(255, 255, 255, 255);
    }
}

void computeReduction(const int outputSize, PositionAndColor* output, const int inputSize, const float2* input, const float2 inputRange, const int colormapSize, const float4* colormap) {
    int block = BLOCK_SIZE;
    int grid = (inputSize + 1) / (block + 1) + 1;
    int* weights = 0;
    cudaMalloc(&weights, sizeof(int) * outputSize);
    cudaMemset(weights, 0, sizeof(int) * outputSize);
    cudaComputeReduction<<<grid, block>>>(outputSize, output, weights, inputSize, input, inputRange, colormapSize, colormap);
    cudaFree(weights);
}
