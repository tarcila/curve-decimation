#version 450 core

layout(location=0) uniform ivec2 viewportSize;

//layout(location=0) in float positionX;
//layout(location=1) in float positionY;

layout(location=0) in vec2 position;

void main() {
    //gl_Position = vec4(2.0 * positionX - 1.0, 2.0 * positionY - 1.0, 0.0, 1.0);
    //gl_Position = vec4(2.0 * position.x - 1.0, 2.0 * position.y - 1.0, 0.0, 1.0);
    gl_Position = vec4(2.0 * position.x - 1.0, 2.0 * position.y - 1.0, 0.0, 1.0);
}
