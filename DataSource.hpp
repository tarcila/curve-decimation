#pragma once

#include <QObject>
#include <QVector>
#include <QVector2D>

class DataSource : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DataSource)


    Q_PROPERTY(Type type READ type WRITE setType NOTIFY updated)
    Q_PROPERTY(int size READ size WRITE setSize NOTIFY updated)

    Q_PROPERTY(QVector<QVector2D> data READ data)

    Q_ENUMS(Type)

public:
    enum Type { Sine, Random };

    explicit DataSource(QObject *parent = 0);

    QVector<QVector2D> data() const;

    int size() const;
    void setSize(qlonglong size);

    Type type() const;
    void setType(Type type);


Q_SIGNALS:
    void updated();

public Q_SLOTS:

private Q_SLOTS:
    void resetData();

private:
    mutable QVector<QVector2D> m_data;
    Type m_type;
    int m_size;
};


