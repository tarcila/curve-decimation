#pragma once

#include <QPointer>
#include <QQuickItem>

#include "DataSource.hpp"

class Viewer3DPrivate;
class Viewer3D : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Viewer3D)

    const QScopedPointer<Viewer3DPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Viewer3D)

    Q_PROPERTY(qreal from READ from MEMBER m_from NOTIFY domainChanged)
    Q_PROPERTY(qreal to READ to MEMBER m_to NOTIFY domainChanged)
    Q_PROPERTY(qreal resolution MEMBER m_resolution NOTIFY domainChanged)
    Q_PROPERTY(DataSource* dataSource MEMBER m_dataSource NOTIFY dataSourceChanged)

    qreal m_from, m_to, m_resolution;
    QPointer<DataSource> m_dataSource;

public:
    explicit Viewer3D(QQuickItem *parent = 0);
    virtual ~Viewer3D();

    qreal from() const;
    qreal to() const;

Q_SIGNALS:
    void domainChanged(qreal value);
    void dataSourceChanged(DataSource* dataProvider);

public Q_SLOTS:
    void handleWindowChanged(QQuickWindow* window);
    void handleDataSourceChanged(DataSource* dataSource);
};
