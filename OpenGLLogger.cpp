#include "OpenGLLogger.hpp"
#include "OpenGLLoggerPrivate.hpp"

OpenGLLogger::OpenGLLogger(QObject *parent) :
    QObject(parent),
    d_ptr(new OpenGLLoggerPrivate(this))
{

}

OpenGLLogger::~OpenGLLogger()
{

}

void OpenGLLogger::initialize()
{
    Q_D(OpenGLLogger);
    d->initialize();
}

void OpenGLLogger::finish()
{
    Q_D(OpenGLLogger);
    d->finish();
}
