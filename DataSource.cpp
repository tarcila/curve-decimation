#include "DataSource.hpp"

#include <QDebug>

DataSource::DataSource(QObject *parent) :
    QObject(parent),
    m_type(Sine) {
    connect(this, SIGNAL(updated()), this, SLOT(resetData()));
}

int DataSource::size() const
{
    return m_size;
}

void DataSource::setSize(qlonglong size)
{
    m_size = size;
}

DataSource::Type DataSource::type() const
{
    return m_type;
}

void DataSource::setType(DataSource::Type type)
{
    m_type = type;
}

void DataSource::resetData() {
    m_data.resize(0);
}

QVector<QVector2D> DataSource::data() const {
    if (m_data.size() != m_size) {
        m_data.clear();
        switch (m_type) {
        case Sine: {
            double N = 5.0 / m_size * M_PI;
            double x = 0.0;
            std::generate_n(std::back_inserter(m_data), m_size, [&]() {
               double nx = x * N;
               return QVector2D(x++, (cos(2 * nx) * sin(nx / 3) + cos(10 * nx) / 4.0) / 1.25 * 0.45 + 0.5);
            });
            break;
        }
        case Random: {
            double x = 0.0;
            std::generate_n(std::back_inserter(m_data), m_size, [&]() {
                return QVector2D(x++, double(std::rand()) / RAND_MAX);
            });
        }
        }
    }

    return m_data;
}

