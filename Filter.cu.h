#pragma once
#include <vector_types.h>

struct PositionAndColor {
    ushort2 position; // half float
    uchar4 color;
};

void computeReduction(const int outputSize, PositionAndColor* output, const int inputSize, const float2* input, const float2 inputRange, const int colormapSize, const float4* colormap);
