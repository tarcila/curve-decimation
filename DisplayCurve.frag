#version 450 core

#define CURVE 0
#define FILL 1
#define DRAW CURVE


layout(location=0) uniform ivec2 viewportSize;
//layout(location=1) uniform float lineWidth = 1.0;

//struct CurveSample {
//  float value;
//  //int color;
//};
//
//layout(std430, binding=0) readonly buffer CurveData {
//    CurveSample samples[];
//} curveData;
//
//in float yPos;
//
layout(location=0) out vec4 color;

void main() {
    color = vec4(1.0);
}

//
//float distanceToLine(vec2 p, vec2 a, vec2 b) {
//  vec2 dir = normalize(b - a);
//  float segmentPos = dot(p-a, dir);
//  float segmentLength = length(b - a);
//  vec2 ph = a + dir * segmentPos;
//  if (segmentPos < 0)
//      return distance(p, a);
//  else if (segmentPos > segmentLength)
//      return distance(p, b);
//  else
//      return distance(p, ph);
//}
//
//void main() {
//  int coord = int(gl_FragCoord.x) * 2 + 1;
//#if (DRAW == CURVE)
//  float d = 10000.0;
//  for (int i = 0; i <= int(lineWidth + 0.5); ++i) {
//      if (gl_FragCoord.x > 1 + i) {
//        float value = curveData.samples[coord - i * 2].value * viewportSize.y;
//        float value_1 = curveData.samples[coord - i * 2 - 2].value * viewportSize.y;
//        vec2 p = vec2(gl_FragCoord.x, yPos * viewportSize.y);
//        vec2 a = vec2(gl_FragCoord.x - 1, value_1);
//        vec2 b = vec2(gl_FragCoord.x, value);
//        d = min(d, distanceToLine(p, a, b));
//      }
//
//      if (gl_FragCoord.x < viewportSize.x - 1 - i) {
//          float value = curveData.samples[coord + i * 2].value * viewportSize.y;
//          float value_1 = curveData.samples[coord + i * 2 + 2].value * viewportSize.y;
//          vec2 p = vec2(gl_FragCoord.x, yPos * viewportSize.y);
//          vec2 a = vec2(gl_FragCoord.x - 1, value_1);
//          vec2 b = vec2(gl_FragCoord.x, value);
//          d = min(d, distanceToLine(p, a, b));
//      }
//  }
//
//  d /= (lineWidth / 2.0);
//
//  if (d <= 1.0) {
//    float alpha = exp2(-2.0 * d * d);
//    color = vec4(1.0, 0.0, 0.0, 1.0);
//   } else
//    discard;
//#elif(DRAW == FILL)
//  float value = curveData.samples[coord].value * viewportSize.y;
//  float yPos2 = yPos * viewportSize.y;
//  if (yPos2 <= value)
//    color = vec4(1.0, 0.0, 0.0, 1.0);
//  else
//    discard;
//#endif
//}
