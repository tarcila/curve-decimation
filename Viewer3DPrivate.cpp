#include "Viewer3DPrivate.hpp"
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QQuickWindow>
#include <QTimer>
#include <QThread>

#include "OpenGLLogger.hpp"

#include <half.hpp>

#include <iterator>
#include <algorithm>

Viewer3DPrivate::Viewer3DPrivate(Viewer3D *parent) :
    q_ptr(parent),
    m_transientInputSize(1024),
    m_transientInput(nullptr),
    m_cudaOutputGraphicalResource(nullptr),
    m_colorMap(0),
    m_frameBuffer(0),
    m_drawCurveProgram(0),
    m_curveDataBuffer(0),
    m_curveVao(0),

    m_antialiasingProgram(0),
    m_antialiasingTexture(0),
    m_normalizedQuadVao(0),

    m_logger(this),
    m_rebuildShaders(true),
    m_sourceWatcher(new QFileSystemWatcher(this)) {
}

void Viewer3DPrivate::compileShader(QString fileName, GLuint shaderObject)
{
    QFile shaderFile(fileName);

    shaderFile.open(QFile::ReadOnly);
    QByteArray shaderCode = shaderFile.readAll();
    const char* p = shaderCode.constData();
    glShaderSource(shaderObject, 1, &p, nullptr);
    glCompileShader(shaderObject);
    char buffer[4096];
    GLsizei length = 0;
    glGetShaderInfoLog(shaderObject, sizeof(buffer) - 1, &length, buffer);
    buffer[length] = '\0';
    qDebug() << fileName << " compile log:\n" << buffer;
}

void Viewer3DPrivate::doConnect()
{
    Q_Q(Viewer3D);

    connect(q->window(), SIGNAL(beforeSynchronizing()), this, SLOT(synchronize()), Qt::DirectConnection);
    connect(q->window(), SIGNAL(sceneGraphInitialized()), this, SLOT(init()), Qt::DirectConnection);
    connect(q->window(), SIGNAL(beforeRendering()), this, SLOT(paint()), Qt::DirectConnection);
    connect(q->window(), SIGNAL(sceneGraphAboutToStop()), this, SLOT(finish()), Qt::DirectConnection);
}

void Viewer3DPrivate::doDisconnect()
{
    disconnect(this, SLOT(finish()));
    disconnect(this, SLOT(paint()));
    disconnect(this, SLOT(init()));
}

void Viewer3DPrivate::init() {
    Q_Q(Viewer3D);

    m_viewport = QRect(q->x(), q->window()->height() - q->height() + q->y(), q->width(), q->height());

    glewInit();

    m_logger.initialize();

    // CUDA computational part
    cudaMalloc(&m_transientInput, m_transientInputSize * sizeof(float2));
    cudaMalloc(&m_colorMap, sizeof(float4) * 256);
    std::vector<float4> colors(256);
    char r = 255, g = 0, b = 255, a =255;
    std::generate(colors.begin(), colors.end(), [&]() {
        return make_float4(r--/255.0f, g++/255.0f, b/255.0f, a/255.0f);
    });
    cudaMemcpy(m_colorMap, colors.data(),sizeof(float4) * colors.size(), cudaMemcpyHostToDevice);

    // Create quad used to support curve rendering
    GLuint vertexBuffer;
    glCreateBuffers(1, &vertexBuffer);
    QVector2D vertices[] = {{1.0f, -1.0f}, {-1.0f, -1.0f}, {1.0f, 1.0f}, {-1.0f, 1.0f}};
    glNamedBufferStorage(vertexBuffer, 4 * sizeof(QVector2D), vertices, 0);

    glCreateVertexArrays(1, &m_normalizedQuadVao);
    glEnableVertexArrayAttrib(m_normalizedQuadVao, 0);
    glVertexArrayVertexBuffer(m_normalizedQuadVao, 0, vertexBuffer, 0, sizeof(QVector2D));
    glVertexArrayAttribFormat(m_normalizedQuadVao, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(m_normalizedQuadVao, 0, 0);

    // Create curve data resources: output of cuda computation and source of lineset
    glCreateBuffers(1, &m_curveDataBuffer);
    glNamedBufferStorage(m_curveDataBuffer, sizeof(PositionAndColor) * m_viewport.width(), nullptr, GL_DYNAMIC_STORAGE_BIT);
    cudaGraphicsGLRegisterBuffer(&m_cudaOutputGraphicalResource, m_curveDataBuffer, cudaGraphicsMapFlagsWriteDiscard);

    glCreateVertexArrays(1, &m_curveVao);
    glEnableVertexArrayAttrib(m_curveVao, 0);
    glVertexArrayVertexBuffer(m_curveVao, 0, m_curveDataBuffer, 0, sizeof(PositionAndColor));
    glVertexArrayAttribFormat(m_curveVao, 0, 2, GL_HALF_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(m_curveVao, 0, 0);

    // Offscreen rendering support
    glCreateTextures(GL_TEXTURE_2D, 1, &m_antialiasingTexture);
    glTextureStorage2D(m_antialiasingTexture, 1, GL_RGBA32F, m_viewport.width(), m_viewport.height());

    glCreateFramebuffers(1, &m_frameBuffer);
    glNamedFramebufferTexture(m_frameBuffer, GL_COLOR_ATTACHMENT0, m_antialiasingTexture, 0);


    // Create curve rendering program
#if 0
    m_drawCurveProgram = glCreateProgram();
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    compileShader(QStringLiteral(":/DisplayCurve.vert"), vertexShader);
    compileShader(QStringLiteral(":/DisplayCurve.frag"), fragmentShader);
    glAttachShader(m_drawCurveProgram, vertexShader);
    glAttachShader(m_drawCurveProgram, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    glLinkProgram(m_drawCurveProgram);
#else
    // Defered to rendering time to be able to track source changes.
    QDir dirName = QFileInfo(__FILE__).absoluteDir();
    QString vertexSource = QFileInfo(dirName, QStringLiteral("DisplayCurve.vert")).absoluteFilePath();
    QString fragmentSource = QFileInfo(dirName, QStringLiteral("DisplayCurve.frag")).absoluteFilePath();
    m_sourceWatcher->addPath(vertexSource);
    m_sourceWatcher->addPath(fragmentSource);
    connect(m_sourceWatcher, &QFileSystemWatcher::fileChanged, [&,w=q->window()]() {
        m_rebuildShaders = true;
        w->update();
    });
    m_rebuildShaders = true;
#endif
}

void Viewer3DPrivate::paint() {
    Q_Q(Viewer3D);

    glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);
    glPushAttrib(GL_ALL_ATTRIB_BITS);

#if 1
    if (m_rebuildShaders) {
        glDeleteProgram(m_drawCurveProgram);
        m_drawCurveProgram = glCreateProgram();
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        QDir dirName = QFileInfo(__FILE__).absoluteDir();
        QString vertexSource = QFileInfo(dirName, QStringLiteral("DisplayCurve.vert")).absoluteFilePath();
        compileShader(vertexSource, vertexShader);
        QString fragmentSource = QFileInfo(dirName, QStringLiteral("DisplayCurve.frag")).absoluteFilePath();
        compileShader(fragmentSource, fragmentShader);

        m_sourceWatcher->removePath(vertexSource);
        m_sourceWatcher->removePath(fragmentSource);
        m_sourceWatcher->addPath(vertexSource);
        m_sourceWatcher->addPath(fragmentSource);

        glAttachShader(m_drawCurveProgram, vertexShader);
        glAttachShader(m_drawCurveProgram, fragmentShader);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        glLinkProgram(m_drawCurveProgram);
        char buffer[4096];
        GLsizei length;
        glGetProgramInfoLog(m_drawCurveProgram, sizeof(buffer) - 1, &length, buffer);
        buffer[length] = '\0';
        qDebug() << "curve program link log:\n" << buffer;

        glDeleteProgram(m_antialiasingProgram);
        m_antialiasingProgram = glCreateProgram();
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        dirName = QFileInfo(__FILE__).absoluteDir();
        vertexSource = QFileInfo(dirName, QStringLiteral("AntiAlias.vert")).absoluteFilePath();
        compileShader(vertexSource, vertexShader);
        fragmentSource = QFileInfo(dirName, QStringLiteral("AntiAlias.frag")).absoluteFilePath();
        compileShader(fragmentSource, fragmentShader);

        m_sourceWatcher->removePath(vertexSource);
        m_sourceWatcher->removePath(fragmentSource);
        m_sourceWatcher->addPath(vertexSource);
        m_sourceWatcher->addPath(fragmentSource);

        glAttachShader(m_antialiasingProgram, vertexShader);
        glAttachShader(m_antialiasingProgram, fragmentShader);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        glLinkProgram(m_antialiasingProgram);
        glGetProgramInfoLog(m_antialiasingProgram, sizeof(buffer) - 1, &length, buffer);
        buffer[length] = '\0';
        qDebug() << "antialias program link log:\n" << buffer;

        m_rebuildShaders = false;
#endif
    }


    glProgramUniform2i(m_drawCurveProgram, 0, m_viewport.width(), m_viewport.height());
    glViewport(m_viewport.x(), m_viewport.y(), m_viewport.width(), m_viewport.height());

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QVector<QVector2D> data = q->m_dataSource->data();

    cudaGraphicsMapResources(1, &m_cudaOutputGraphicalResource);
    PositionAndColor* output;
    std::size_t numBytes;
    cudaGraphicsResourceGetMappedPointer(reinterpret_cast<void**>(&output), &numBytes, m_cudaOutputGraphicalResource);
    std::size_t m_outputSize = numBytes / sizeof(PositionAndColor);

    std::size_t from = q->from();
    std::size_t remaining = q->to() - from;

    auto range = make_float2(data[from].x(), data[from + remaining - 1].x());

    while (remaining > m_transientInputSize) {
        cudaMemcpy(m_transientInput, data.constData() + from, sizeof(float2) * m_transientInputSize, cudaMemcpyHostToDevice);
        computeReduction(m_outputSize, output, m_transientInputSize, m_transientInput, range, 256, m_colorMap);
        from += m_transientInputSize;
        remaining -= m_transientInputSize;
    }

    cudaMemcpy(m_transientInput, data.constData() + from, sizeof(float2) * remaining, cudaMemcpyHostToDevice);
    computeReduction(m_outputSize, output, remaining, m_transientInput, range, 256, m_colorMap);

    cudaGraphicsUnmapResources(1, &m_cudaOutputGraphicalResource);


    float zero[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    glClearTexImage(m_antialiasingTexture, 0, GL_RGBA, GL_FLOAT, zero);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_frameBuffer);
    glUseProgram(m_drawCurveProgram);
    glBindVertexArray(m_curveVao);
    glDrawArrays(GL_LINE_STRIP, 0, m_viewport.width());
    glBindVertexArray(0);
    glUseProgram(0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    glUseProgram(m_antialiasingProgram);
    glBindVertexArray(m_normalizedQuadVao);
    glBindTexture(GL_TEXTURE_2D, m_antialiasingTexture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    glPopAttrib();
    glPopClientAttrib();
}

void Viewer3DPrivate::finish()
{
    if (m_cudaOutputGraphicalResource)
        cudaGraphicsUnregisterResource(m_cudaOutputGraphicalResource);
    cudaFree(m_colorMap);
    cudaFree(m_transientInput);

    glDeleteVertexArrays(1, &m_normalizedQuadVao);
    glDeleteTextures(1, &m_antialiasingTexture);
    glDeleteProgram(m_antialiasingProgram);

    glDeleteVertexArrays(1, &m_curveVao);

    glDeleteBuffers(1, &m_curveDataBuffer);
    glDeleteProgram(m_drawCurveProgram);

    glDeleteFramebuffers(1, &m_frameBuffer);

    m_logger.finish();
}

void Viewer3DPrivate::synchronize()
{
    Q_Q(Viewer3D);
    QRect newViewport = QRect(q->x(), q->window()->height() - q->height() + q->y(), q->width(), q->height());
    bool viewportChanged = m_viewport != newViewport;
    m_viewport = newViewport;

    if (viewportChanged) {
        cudaGraphicsUnregisterResource(m_cudaOutputGraphicalResource);
        glDeleteBuffers(1, &m_curveDataBuffer);
        glCreateBuffers(1, &m_curveDataBuffer);
        glNamedBufferStorage(m_curveDataBuffer, sizeof(PositionAndColor) * m_viewport.width(), nullptr, GL_DYNAMIC_STORAGE_BIT);
        glVertexArrayVertexBuffer(m_curveVao, 0, m_curveDataBuffer, 0, sizeof(PositionAndColor));

        cudaGraphicsGLRegisterBuffer(&m_cudaOutputGraphicalResource, m_curveDataBuffer, cudaGraphicsMapFlagsWriteDiscard);

        glDeleteTextures(1, &m_antialiasingTexture);
        glCreateTextures(GL_TEXTURE_2D, 1, &m_antialiasingTexture);
        glTextureStorage2D(m_antialiasingTexture, 1, GL_RGBA32F, m_viewport.width(), m_viewport.height());
        glNamedFramebufferTexture(m_frameBuffer, GL_COLOR_ATTACHMENT0, m_antialiasingTexture, 0);
    }

    // do CUDA here ?
}
